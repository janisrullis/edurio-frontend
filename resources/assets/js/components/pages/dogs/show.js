module.exports = {

  data: function () {
    return {
      dog: {
        id: null,
        name: null,
        code: null,
        color: null,
        age: null
      },
      messages: []
    }
  },

  methods: {
    fetch: function (id, successHandler) {
      var that = this
      client({ path: '/dogs/' + id }).then(
        function (response) {
          that.$set('dog', response.entity.data)
          successHandler(response.entity.data)
        },
        function (response, status, request) {
          if (status === 401) {
            self.$dispatch('userHasLoggedOut')
          } else {
            console.log(response)
          }
        }
      )
    },

    updateDog: function (e) {
      e.preventDefault()
      var self = this
      client({ path: '/dogs/' + this.dog.id, entity: this.dog, method: 'PUT'}).then(
        function (response) {
          self.messages = []
          self.messages.push({type: 'success', message: 'Updated'})
        },
        function (response) {
          self.messages = []
          for (var key in response.entity) {
            self.messages.push({type: 'danger', message: response.entity[key]})
          }
        }
      )
    }

  },

  route: {
    data: function (transition) {
      this.fetch(this.$route.params.id, function (data) {
        transition.next({dog: data})
      })
    }
  }
}
