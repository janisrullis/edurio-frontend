module.exports = {
  data: function () {
    return {
      user: {
        name: '',
        email: '',
        password: '',
        password_confirmation: ''
      },
      messages: [],
      creating: false
    }
  },

  methods: {
    createUser: function (e) {
      e.preventDefault()
      var that = this
      that.creating = true
      client({path: 'users', entity: this.user}).then(
        function (response, status) {
          that.user.name = ''
          that.user.age = ''
          that.messages = [ {type: 'success', message: 'Created'} ]
          Vue.nextTick(function () {
            document.getElementById('nameInput').focus()
          })
          that.creating = false
        },
        function (response, status) {
          that.messages = []
          for (var key in response.entity) {
            that.messages.push({type: 'danger', message: response.entity[key]})
            that.creating = false
          }
        }
      )
    }
  }
}
