module.exports = {
    data: function () {
        return {
            messages: [],
        }
    },
    methods: {
        importDogs: function (e) {

            var that = this;
            var formData = new FormData();
            formData.append('file', e.target.files[0]);
            var token = localStorage.getItem('jwt-token');

            $.ajax({
                beforeSend: function (xhrObj) {
                    xhrObj.setRequestHeader("Authorization", 'Bearer: ' + token);
                },
                url: config.api.base_url + '/dogs/import',
                data: formData,
                type: "POST",
                headers: {
                    'Authorization': 'Bearer: ' + token
                },
                // THIS MUST BE DONE FOR FILE UPLOADING
                contentType: false,
                processData: false,
                success: function (response) {

                    if (response === 'success') {

                        that.messages.push({type: 'success', message: 'Imported!'});
                    } else {

                        that.messages.push({type: 'danger', message: 'Import failed'});
                    }

                    // Hide the import message after few seconds
                    setTimeout(function () {

                        that.messages = [];
                    }, 2000);
                }
            });

        }
    }

}
