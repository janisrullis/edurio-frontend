## Front-end
[Working example](http://edurio.mywebapp.lv)

[Repo](https://jrullis@bitbucket.org/janisrullis/edurio-frontend.git)

## Setup

```
git clone https://jrullis@bitbucket.org/janisrullis/edurio-frontend.git
sudo chown www-data:www-data edurio-frontend -R
sudo chmod a+rwX edurio-frontend -R
cd edurio-frontend
npm install
gulp
```

## Config
By default the backend API address is set to 'http://backend.edurio.mywebapp.lv', but if 
you want to change it, then you can do it in resources/assets/js/config/development.config.js
and change the 'base_url'
```
base_url: 'http://backend.edurio.mywebapp.lv',
```

## Where to look?
See resources/assets/js/. It's where the main magic happens.

## Back-end
[Working example](http://backend.edurio.mywebapp.lv)

[Repo](https://jrullis@bitbucket.org/janisrullis/edurio-backend.git)

## License
MIT License. See LICENSE file.

## Credits
Thanks to Koen Calliauw @kcalliauw for https://github.com/layer7be/vue-starter.
This repo is mostly based on it.